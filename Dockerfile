FROM centos:centos8
MAINTAINER rleigh@codelibre.net

RUN dnf -y install epel-release && dnf -y update && dnf -y clean all

RUN dnf -y install dnf-plugins-core && dnf config-manager --set-enabled powertools && dnf -y update && dnf -y clean all

RUN dnf groupinstall -y "Development Tools"

RUN dnf install -y \
  curl \
  doxygen \
  git-core \
  glibc-langpack-en \
  graphviz \
  jq \
  man-db \
  ninja-build \
  sqlite \
  zip \
  unzip \
  boost-devel \
  fmt-devel \
  spdlog-devel \
  glm-devel \
  hdf5-devel \
  xerces-c-devel \
  libpng-devel \
  gtest-devel \
  qt5-qtbase-devel \
  qt5-qtsvg-devel \
  qt5-qtxmlpatterns-devel \
  sqlite-devel \
  libtiff-devel \
  zlib-devel \
  python38-devel

RUN curl -Lo cmake.tar.gz https://github.com/Kitware/CMake/releases/download/v3.18.4/cmake-3.18.4-Linux-x86_64.tar.gz \
    && echo "149e0cee002e59e0bb84543cf3cb099f108c08390392605e944daeb6594cbc29 *cmake.tar.gz" | sha256sum --check - \
    && cd /usr/local \
    && tar --strip-components=1 -xvf /cmake.tar.gz \
    && rm /cmake.tar.gz

RUN mkdir /opt/venv

RUN /usr/bin/python3.8 -m venv /opt/venv/py38

RUN . /opt/venv/py38/bin/activate && \
  pip install genshi sphinx six numpy

ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
